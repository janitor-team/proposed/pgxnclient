pgxnclient (1.3.1-2) unstable; urgency=medium

  * Mark autopkgtest as needs-internet.

 -- Christoph Berg <myon@debian.org>  Mon, 06 Sep 2021 21:52:04 +0200

pgxnclient (1.3.1-1) unstable; urgency=medium

  * New upstream version.
  * Disable build-time tests.

 -- Christoph Berg <myon@debian.org>  Wed, 02 Dec 2020 12:57:25 +0100

pgxnclient (1.3-1) unstable; urgency=medium

  * New upstream version with python3 support. (#937280)
  * watch: Switch to github releases, the pypi release is missing the
    tests/testdata directory.
  * Add python3-pytest{,-runner} build dependencies.
  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <myon@debian.org>  Mon, 23 Sep 2019 10:04:20 +0200

pgxnclient (1.2.1-6) unstable; urgency=medium

  * Move maintainer address to team+postgresql@tracker.debian.org.

 -- Christoph Berg <christoph.berg@credativ.de>  Wed, 10 Oct 2018 10:13:43 +0200

pgxnclient (1.2.1-5) unstable; urgency=medium

  * Depend on ca-certificates, pgxn.org moved to https.
  * Update homepage.

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 22 May 2018 12:44:55 +0200

pgxnclient (1.2.1-4) unstable; urgency=medium

  * Update Vcs URLs.
  * Compat 11.
  * Generate manpage using help2man.
  * Add test searching for the 'debversion' extension.

 -- Christoph Berg <myon@debian.org>  Sat, 19 May 2018 16:55:14 +0200

pgxnclient (1.2.1-3) unstable; urgency=medium

  * Set HOME to a temp dir for the testsuite. Closes: #759911.
  * Set team as maintainer.

 -- Christoph Berg <myon@debian.org>  Sun, 07 Sep 2014 16:11:16 +0200

pgxnclient (1.2.1-2) unstable; urgency=low

  * B-D on libpq-dev to make pg_config available. Spotted by Felix Geyer.
    Closes: #713054.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Jun 2013 10:40:28 +0200

pgxnclient (1.2.1-1) unstable; urgency=low

  * New upstream release.
  * Add B-D python-simplejson, python-unittest2 for the testsuite (needed on
    squeeze).
  * Point Vcs-Browser at a real webpage instead of some repository.

 -- Christoph Berg <myon@debian.org>  Thu, 20 Jun 2013 12:25:53 +0200

pgxnclient (1.2-1) experimental; urgency=low

  * New upstream release.
  * Install AUTHORS file.
  * Point watch file directly at the download directory.

 -- Christoph Berg <myon@debian.org>  Sat, 06 Oct 2012 10:50:15 +0200

pgxnclient (1.0.3-1) unstable; urgency=low

  * New upstream release.

 -- Christoph Berg <myon@debian.org>  Tue, 22 May 2012 12:11:53 +0200

pgxnclient (1.0.2-2) unstable; urgency=low

  * Add watch file.
  * Add dependency on python-pkg-resources, spotted by Jakub Wilk.
    Closes: #672904.
  * Add build-dependency on python-mock so we don't download it at build time.

 -- Christoph Berg <myon@debian.org>  Thu, 05 Apr 2012 13:56:11 +0200

pgxnclient (1.0.2-1) unstable; urgency=low

  * Initial release.

 -- Christoph Berg <myon@debian.org>  Wed, 04 Apr 2012 21:43:55 +0200
